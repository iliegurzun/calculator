<?php

namespace App\Controller;

use App\Form\CalculatorType;
use App\Service\Calculator;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Throwable;

class AppController extends AbstractController
{
    /**
     * @Template("calculator.html.twig")
     * @Route("/calculator")
     */
    public function calculator(Calculator $calculator, Request $request): array
    {
        $form = $this->createForm(CalculatorType::class);
        $form->handleRequest($request);
        $result = null;
        if ($form->isSubmitted() && $form->isValid()) {
            $result = $calculator->calculate(
                $form->get('firstNumber')->getData(),
                $form->get('secondNumber')->getData(),
                $form->get('method')->getData()
            );
        }

        return [
            'form' => $form->createView(),
            'result' => $result,
        ];
    }
}
