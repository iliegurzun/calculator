<?php

namespace App\Form;

use App\Service\CalculationMethod\DivideMethod;
use App\Service\Calculator;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

class CalculatorType extends AbstractType
{
    protected Calculator $calculator;

    public function __construct(Calculator $calculator)
    {
        $this->calculator = $calculator;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstNumber', NumberType::class, [
                'required' => true,
                'constraints' => [
                    new Assert\NotBlank(),
                ],
            ])
            ->add('secondNumber', NumberType::class, [
                'required' => true,
                'constraints' => [
                    new Assert\NotBlank(),
                ],
            ])
            ->add('method', ChoiceType::class, [
                'required' => true,
                'choices' => $this->calculator->getMethods(),
                'constraints' => [
                    new Assert\NotBlank(),
                ],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'constraints' => [
                new Assert\Callback(function (
                    $data,
                    ExecutionContextInterface $context
                ) {
                    $propertyAccessor = PropertyAccess::createPropertyAccessor();
                    $method = $propertyAccessor->getValue($data, '[method]');
                    $secondNumber = $propertyAccessor->getValue($data, '[secondNumber]');
                    if (null === $secondNumber || null === $method) {
                        return;
                    }
                    if (0.0 === $secondNumber && DivideMethod::METHOD_NAME === $method) {
                        $context
                            ->buildViolation('You cannot divide by zero.')
                            ->atPath('[secondNumber]')
                            ->addViolation();
                    }
                }),
            ],
        ]);
    }
}
