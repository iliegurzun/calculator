<?php

namespace App\Exception;

use Exception;

class CalculationMethodNotImplemented extends Exception
{
}
