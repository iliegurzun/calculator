<?php

namespace App\CompilerPass;

use App\Service\Calculator;
use Exception;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

class CalculationMethodsCompilerPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container): void
    {
        if (!$container->has(Calculator::class)) {
            throw new Exception('Calculator service does not exist in the container.');
        }

        $calculator = $container->findDefinition(Calculator::class);

        $methods = $container->findTaggedServiceIds('calculation_method');

        foreach ($methods as $id => $tags) {
            $calculator->addMethodCall('addMethod', [new Reference($id)]);
        }
    }
}
