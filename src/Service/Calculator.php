<?php

namespace App\Service;

use App\Exception\CalculationMethodNotImplemented;
use App\Service\CalculationMethod\AbstractCalculationMethod;

class Calculator
{
    protected array $methods = [];

    public function addMethod(AbstractCalculationMethod $calculationMethod)
    {
        $this->methods[$calculationMethod->getMethodName()] = $calculationMethod;
    }

    public function getMethod(string $method): AbstractCalculationMethod
    {
        if (!isset($this->methods[$method])) {
            throw new CalculationMethodNotImplemented();
        }

        return $this->methods[$method];
    }

    public function getMethods(): array
    {
        $methods = [];
        foreach ($this->methods as $key => $method) {
            $methods[ucfirst($key)] = $key;
        }

        return $methods;
    }

    public function calculate(float $firstNumber, float $secondNumber, string $method): float
    {
        return $this->getMethod($method)->calculate($firstNumber, $secondNumber);
    }
}
