<?php

namespace App\Service\CalculationMethod;

class SubtractMethod extends AbstractCalculationMethod
{
    public function calculate(float $firstNumber, float $secondNumber): float
    {
        return $firstNumber - $secondNumber;
    }

    public function getMethodName(): string
    {
        return 'subtract';
    }
}
