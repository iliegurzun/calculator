<?php

namespace App\Service\CalculationMethod;

abstract class AbstractCalculationMethod
{
    abstract public function calculate(float $firstNumber, float $secondNumber): float;

    abstract public function getMethodName(): string;
}
