<?php

namespace App\Service\CalculationMethod;

class DivideMethod extends AbstractCalculationMethod
{
    public const METHOD_NAME = 'divide';

    public function calculate(float $firstNumber, float $secondNumber): float
    {
        return $firstNumber / $secondNumber;
    }

    public function getMethodName(): string
    {
        return self::METHOD_NAME;
    }
}
