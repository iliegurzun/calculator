<?php

namespace App\Service\CalculationMethod;

class MultiplyMethod extends AbstractCalculationMethod
{
    public function calculate(float $firstNumber, float $secondNumber): float
    {
        return $firstNumber * $secondNumber;
    }

    public function getMethodName(): string
    {
        return 'multiply';
    }
}
