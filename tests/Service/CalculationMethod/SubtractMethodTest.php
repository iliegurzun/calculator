<?php

namespace App\Tests\Service\CalculationMethod;

use App\Service\CalculationMethod\SubtractMethod;
use PHPUnit\Framework\TestCase;

class SubtractMethodTest extends TestCase
{
    /**
     * @dataProvider subtractCalculateProvider
     */
    public function testCalculate(float $first, float $second, float $expected)
    {
        $method = new SubtractMethod();
        $this->assertEquals($expected, $method->calculate($first, $second));
    }

    public function subtractCalculateProvider(): array
    {
        return [
            [
                10,
                2,
                8
            ]
        ];
    }
}
