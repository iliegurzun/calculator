<?php

namespace App\Tests\Service\CalculationMethod;

use App\Service\CalculationMethod\SumMethod;
use Monolog\Test\TestCase;

class SumMethodTest extends TestCase
{
    /**
     * @dataProvider sumCalculateProvider
     */
    public function testCalculate(float $first, float $second, float $expected)
    {
        $method = new SumMethod();
        $this->assertEquals($expected, $method->calculate($first, $second));
    }

    public function sumCalculateProvider(): array
    {
        return [
            [
                10,
                2,
                12
            ],
        ];
    }
}
