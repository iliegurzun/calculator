<?php

namespace App\Tests\Service\CalculationMethod;

use App\Service\CalculationMethod\DivideMethod;
use PHPUnit\Framework\TestCase;

class DivideMethodTest extends TestCase
{
    /**
     * @dataProvider divideCalculateProvider
     */
    public function testCalculate(float $first, float $second, float $expected)
    {
        $method = new DivideMethod();
        $this->assertEquals($expected, $method->calculate($first, $second));
    }

    public function divideCalculateProvider(): array
    {
        return [
            [
                10,
                2,
                5
            ]
        ];
    }
}
