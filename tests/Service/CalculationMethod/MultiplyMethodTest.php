<?php

namespace App\Tests\Service\CalculationMethod;

use App\Service\CalculationMethod\MultiplyMethod;
use PHPUnit\Framework\TestCase;

class MultiplyMethodTest extends TestCase
{
    /**
     * @dataProvider multiplyCalculateProvider
     */
    public function testCalculate(float $first, float $second, float $expected)
    {
        $method = new MultiplyMethod();
        $this->assertEquals($expected, $method->calculate($first, $second));
    }

    public function multiplyCalculateProvider(): array
    {
        return [
            [
                10,
                2,
                20
            ],
        ];
    }
}
