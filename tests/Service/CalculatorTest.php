<?php

namespace App\Tests\Service;

use App\Exception\CalculationMethodNotImplemented;
use App\Service\CalculationMethod\AbstractCalculationMethod;
use App\Service\CalculationMethod\DivideMethod;
use App\Service\Calculator;
use Monolog\Test\TestCase;

class CalculatorTest extends TestCase
{
    protected Calculator $calculator;

    public function setUp(): void
    {
        $this->calculator = new Calculator();
    }

    public function testGetMethod()
    {
        $this->expectException(CalculationMethodNotImplemented::class);
        $this->calculator->getMethod('foo');
    }

    /**
     * @dataProvider calculateProvider
     */
    public function testCalculate(AbstractCalculationMethod $calculationMethod, $firstNumber, $secondNumber, $expected)
    {
        $this->calculator->addMethod($calculationMethod);
        $this->assertEquals(
            $expected,
            $this->calculator->calculate($firstNumber, $secondNumber, $calculationMethod->getMethodName())
        );
    }

    public function calculateProvider(): array
    {
        return [
            [
                new DivideMethod(),
                100,
                2,
                50,
            ]
        ];
    }
}
